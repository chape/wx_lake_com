'use strict'
import Promise from '../lib/promise'
import config from './config'
import utils from './util'
import * as Mock from './mock'
import app from '../app'

const DEFAULT_REQUEST_OPTIONS = {
  url: '',
  data: {},
  header: {
    "Content-Type": "application/json"
  },
  method: 'GET',
  dataType: 'json'
}

let util = {
  isDEV: config.isDev,
  log() {
    this.isDEV && console.log(...arguments)
  },
  alert(title = '提示', content = config.defaultAlertMsg) {
    if ('object' === typeof content) {
      content = this.isDEV && JSON.stringify(content) || config.defaultAlertMsg
    }
    wx.showModal({
      title: title,
      content: content
    })
  },
  templateFunc(str, data){
    let computed = str.replace(/\{(\w+)\}/g, function (match, key) {
      return data[key];
    })
    return computed;
  },
  getStorageData(key, cb) {
    let self = this;

    // 将数据存储在本地缓存中指定的 key 中，会覆盖掉原来该 key 对应的内容，这是一个异步接口
    wx.getStorage({
      key: key,
      success(res) {
        cb && cb(res.data);
      },
      fail(err) {
        let msg = err.errMsg || '';
        if (/getStorage:fail/.test(msg)) {
          self.setStorageData(key)
        }
      }
    })
  },
  setStorageData(key, value = '', cb) {
    wx.setStorage({
      key: key,
      data: value,
      success() {
        cb && cb();
      }
    })
  },
  request(opt) {
    let options = Object.assign({}, DEFAULT_REQUEST_OPTIONS, opt)
    let { url, data, header, method, dataType, mock = false } = options
    let self = this
    return new Promise((resolve, reject) => {
      if (mock) {
        let res = {
          statusCode: 200,
          data: Mock[url]
        }
        if (res && res.statusCode == 200 && res.data) {
          resolve(res.data);
        } else {
          self.alert('提示', res);
          reject(res);
        }
      } else {
        console.log(header)
          // if (utils.hasText(header['x-auth-token'])){
          //     wx.redirectTo({
          //         url: '/pages/auth/auth'
          //     });
          // }

        wx.request({
          url: url,
          data: data,
          header: header,
          method: method,
          dataType: dataType,
          success: function (res) {
              if (res.data.code === 4007 || res.data.code === 30000 || res.data.code === 30001) {
                wx.removeStorageSync('sessionId');
                app.login();
              }
            if (res && res.statusCode == 200 && res.data) {
              resolve(res.data);
            } else {
              self.alert('提示', res);
              reject(res);
            }
          },
          fail: function (err) {
            console.log(res)
            self.log(err);
            self.alert('提示', err);
            reject(err);
          }
        })
      }
    })
  }
}
export default util