import api from 'api';
/**
 * 校验登录
 */
var checkLogin = (success, fail) => {
    var authToken = wx.getStorageSync("sessionId");
    if (!authToken) {
        typeof fail == "function" && fail();
    } else {
        wx.checkSession({
            success: function () {
                wx.request({
                    url: api.getUserUrl,
                    header: {
                      'content-type': 'application/json',
                      'x-auth-token': authToken
                    },
                    complete: function (res) {
                      //失败
                      if (res.statusCode != 200 || res.data.errorCode != 0) {
                            typeof fail == "function" && fail();
                      //成功
                        } else {
                            typeof success == "function" && success();
                        }
                    }
                })
            },
            fail: function () {
                typeof fail == "function" && fail();
            }
        })
    }
}

/**
 * 登录
 */
var login = (success, fail) => {
    checkLogin(() => {
        console.log("已登录");
    }, () => {
        remoteLogin(success, fail)
    });
}

/**
 * 服务端请求登录
 */
var remoteLogin = (success, fail) => {
    //调用登录接口
    wx.login({
        success: function (loginRes) {
            console.log("登录获取code", loginRes);
            wx.request({
                url: api.loginUrl,
                data: {
                  code: loginRes.code
                },
                complete: function (res) {
                  //失败
                  if (res.statusCode != 200 || res.data.errorCode != 0) {
                        console.error("登陆失败", res);
                        var data = res.data || { msg: '无法请求服务器' };
                        if (typeof fail == "function") {
                            fail();
                        } else {
                            wx.showModal({
                                title: '提示',
                                content: data.msg,
                                showCancel: false
                            });
                        }
                    } else {//成功
                        console.log("登录成功", res);
                        wx.setStorage({
                            key: "sessionId",
                            data: res.data.data.sessionId
                        })
                        typeof success == "function" && success();
                    }
                }
            })
        }
    })
}

var getUserInfo = (success, fail) => {
  console.log("-------111")
    wx.getUserInfo({
        success: function (res) {
            console.log("获取用户信息", res);
            var userInfo = res.userInfo
            wx.request({
              url: api.postUserUrl,
              method: 'POST',
              header: {
                'content-type': 'application/json',
                'x-auth-token': wx.getStorageSync("sessionId")
              },
                data: {
                    encryptedData: res.encryptedData,
                    iv: res.iv
                }, success: function (reqRes) {
                  console.log("-------")
                  typeof success == "function" && success(reqRes.data.data);
                }
            });
        }, fail: function () {
          console.log("-------111")
            typeof fail == "function" && fail();
        }
    })
}

module.exports = {
    login: login,
    checkLogin: checkLogin,
    getUserInfo: getUserInfo
}