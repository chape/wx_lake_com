// var NewApiRootUrl = 'http://localhost:8083/';
var NewApiRootUrl = 'https://www.lake2716.cn/';

module.exports = {
    notificationUrl: NewApiRootUrl + 'notification/notifications',  //轮播公告列表(GET)
    noticePageUrl: NewApiRootUrl + 'notification/notifications/page',  //公告列表(GET)
    notificationDetails: NewApiRootUrl + 'notification/notifications/{id}',

    postFeedbackUrl: NewApiRootUrl + 'feedback/feedbacks',  //提交用户反馈(POST)
    yellowUrl: NewApiRootUrl + 'yellow/yellows',   //黄页列表(GET)
    
    loginUrl: NewApiRootUrl + 'users/login', //登录接口(POST)
    postUserUrl: NewApiRootUrl + 'users/user', //提交用户信息接口(POST)
    getUserUrl: NewApiRootUrl + 'users/user', //查询用户信息接口(GET)
    checkTokenUrl: NewApiRootUrl + 'users/user/token', //检查token接口(GET)

    homepageProductUrl: NewApiRootUrl + 'product/products',  //首页产品列表(GET)
    homepageProductRecommend: NewApiRootUrl + 'product/products/recommend5',  //首页轮播产品推荐
    productDetailUrl: NewApiRootUrl + 'product/products/{id}', // 产品详情(GET)
    offProductUrl: NewApiRootUrl + 'product/products/{id}', // 下架产品(DELETE)
    onShelfProductUrl: NewApiRootUrl + 'product/products/{id}/onShelf', // 上架产品
    userProductsUrl: NewApiRootUrl + 'product/{userId}/products', // 用户产品列表(GET)
    postProductsUrl: NewApiRootUrl + 'product/products', // 发布产品(POST)
    postProductsUrl: NewApiRootUrl + 'product/products', // 更新发布产品(POST)

    createOrderUrl: NewApiRootUrl + 'order/orders',  // 创建订单(POST)
    orderDetailUrl: NewApiRootUrl + 'order/orders/{code}',  // 订单详情(GET)
    sellOrderUrl: NewApiRootUrl + 'order/{userId}/sell_orders',  // 已卖出订单列表(GET)
    buyOrderUrl: NewApiRootUrl + 'order/{userId}/buy_orders',  // 已买的订单列表(GET)
    productOrderUrl: NewApiRootUrl + 'order/products/{productId}/sell_orders',  // 产品订单列表(GET)
    shipOrderUrl: NewApiRootUrl + 'order/orders/{code}/shiped',  // 订单配送(PUT)


    specItemUrl: NewApiRootUrl + 'spec/specs',  // 规格接口

    // 用户下单通知接口
    uploadToken: NewApiRootUrl + 'accessToken/get',  //七牛上传获取token
     getQRCodeUrl: NewApiRootUrl + 'accessToken/weixin/qrcode'// 获取二维码图片url
};