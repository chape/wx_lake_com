'use strict';

import api from '../../utils/api';
import util from '../../utils/index';
//获取应用实例
const app = getApp()

Page({

  data: {
    userInfo: {}
  },

  // 页面初始化
  onLoad: function (options) {
    this.setData({
      userInfo: app.globalData.userInfo
    });
  },
  renderUser(token) {
    util.request({
      url: api.getUserUrl,
      header: {
        'content-type': 'application/json',
        'x-auth-token': token
      },
    }).then(res => {
      if (res && res.data) {
        console.log(res.data)
        this.setData({
          userInfo: res.data
        });
      }
    })
  }
})
