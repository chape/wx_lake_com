'use strict';

import api from '../../utils/api';
import util from '../../utils/index';
import Blob from '../../lib/Blob.js';

const app = getApp();

Page({
    data:{
        currentId:'', //当前记录的Id
        detailInfo: {},
        orderList: [],
        orderListEmpty: false,
        orderListSize: 0,
        html:'',
        showGoTop: false, //是否显示回到顶部按钮
        isShowDetails: true, //切换购买记录使用
        isShowRecord: false,
        qrCodeUrl: ''
    },

    // 页面初始化
    onLoad:function (options) {
        this.setData({
          currentId: options.id || decodeURIComponent(options.scene)
        });
        this.initProductDetails(this.data.currentId);
    },


    /**
     * 根据ID从服务端获取记录详情
     * @param id
     */
    initProductDetails:function (id) {

        wx.showLoading({
            title: '全力加载中...',
        })

      var that = this;
      //商品详情
      util.request({
        url: util.templateFunc(api.productDetailUrl, { id: id })
      }).then(res=>{
          that.setData({
             detailInfo: res.data,
             html: res.data.description
          });
        })

      //购买列表
      util.request({
        url: util.templateFunc(api.productOrderUrl, { productId: id })
      }).then(res => {
        // 数据正常返回
        if (res && res.data) {
          that.setData({
            orderList: res.data,
            orderListEmpty: res.data.length === 0 ? true :false,
            orderListSize: res.data.length
          });
        }
      })

        wx.hideLoading()

    },

    goByeView:function (e) {
        wx.navigateTo({
            url:'/pages/buy/buy?id='+e.currentTarget.dataset.id.id + '&price='+e.currentTarget.dataset.id.price
        })
    },


    /*
     * 分享
     */
    onShareAppMessage() {
        let title = config.defaultShareText || '';
        return {
            title: title,
            path: "/pages/index/index?id="+this.data.currentId,
            success: function (res) {
                wx.showToast({
                    title: '分享成功',
                    icon: 'success',
                    duration: 1000
                })
            },
            fail: function (res) {
                // 转发失败
            }
        }
    },

    goIndex:function (e) {
        wx.switchTab({
            url: "/pages/index/index",
            // 跳转后刷新界面
            success: function (e) {
                var page = getCurrentPages().pop();
                if (page == undefined || page == null) return;
                page.onLoad();
            }
        });
    },

    // 回到顶部
    goTop:function(e){
        wx.pageScrollTo({
            scrollTop: 0,
            duration: 400
        });
        this.setData({
            showGoTop: false
        })
    },

    // 监听屏幕滚动
    handletouchmove: function (e) {
        this.queryMultipleNodes();
    },
    //获取屏幕滚动出去的高度
    queryMultipleNodes: function () {
        var self = this;
        var query = wx.createSelectorQuery()
        query.select('#header').boundingClientRect()
        query.selectViewport().scrollOffset()
        query.exec(function (res) {

            console.log(res[1].scrollTop);

            //如果顶部的距离超过200   就显示GoTop按钮
            if(res[1].scrollTop > 1000) {
                self.setData({
                    showGoTop: true
                })
            }
            else {
                self.setData({
                    showGoTop: false
                })
            }
        })
    },


    //tab 页切换
    changes:function(e){
        console.log(e.currentTarget.dataset.current)
        this.setData({
            isShowDetails: e.currentTarget.dataset.current === 0 ? true : false,
            isShowRecord: e.currentTarget.dataset.current === 1 ? true : false
        })
    },


//  getQRCodeUrl: function(id){
//    util.request({
//      url: api.getQRCodeUrl,
//      method: 'GET',
//      header: {
//        'content-type': 'application/json',
//        'x-auth-token': wx.getStorageSync('sessionId')
//      },
//      data: {
//        scene: decodeURIComponent(id),           
//        pages: 'pages/index/index'
//      }
//    })
//    .then(res => {
//      // 数据正常返回
//      if (res && res.data) {
//        this.setData({
//          qrCodeUrl: res.data
//        })
//      }
//    })
//  },

//   // 分享
//   shareHandler: function (e) {
//     // 如果已经生成了，那么就显示
//     if (this.SHARE_IMG) {
//       return wx.previewImage({
//         urls: [this.SHARE_IMG],
//       })
//     }
//     var IS_FIRST_SHARE = parseInt(wx.getStorageSync('share_count') || 0);
//     if (IS_FIRST_SHARE < 3) {
//       wx.showToast({
//         title: '生成图片后长按保存分享！',
//         icon: 'success',
//         duration: 1000
//       })
//     }
//     wx.showLoading({
//       title: '生成图片中',
//       mask: true
//     });
//     const ctx = wx.createCanvasContext('shareCanvas', this);
//     // ctx.save();
//     ctx.drawImage('/imgs/share_tpl2@2x.png', 0, 0, 414, 736);
//     //ctx.draw();

//     const {
//       imgUrl, title, shipTime,
//       publisherName, publisherPhone
//     } = this.data.detailInfo;
//     new Promise(RES => {
//       // 下载图片
//       wx.getImageInfo({
//         src: imgUrl,
//         success: ret => {
//           ctx.drawImage(ret.path, 0, 0, 414, 290);
//           // 渲染模板图片
//           ctx.drawImage('/imgs/box@2x.png', 0, 174, 414, 562);
//           RES();
//         }
//       })
//     }).then(() => new Promise(RES => {
//       console.log(imgUrl, 9988, this.data.qrCodeUrl)
//       util.request({
//         url: api.getQRCodeUrl,
//         method: 'GET',
//         header: {
//           'content-type': 'application/json',
//           'x-auth-token': wx.getStorageSync('sessionId')
//         },
//         data: {
//           scene: decodeURIComponent(id),
//           pages: 'pages/index/index'
//         }
//       })
//         .then(res => {
//           // 数据正常返回
//           if (res && res.data) {
//             // 下载动态二维码
//             wx.getImageInfo({
//               src: res.data,
//               success: ret => {
//                 // 渲染二维码
//                 ctx.drawImage(ret.path, 167, 618, 80, 80);
//                 RES();
//               }
//             })
//           }
//         })
      
//     })).then(() => new Promise(RES => {
//       // 绘制文字

//       // 文字
//       /**
//        * 渲染主体内容思路：
//        * 
//        * 首先，通过\r\n进行分割，获取到每一行。
//        * 然后，每一行进行每20*30个方格的计算，多出来的换到下一行，不满足的用空格填充
//        * 综上，一共获取到前三行（换行过长算作下一行）—
//        */

//       // 一行多少字
//       const NUMBER_OF_LINE = 15;
//       // 每个字多宽
//       const FONT_WIDTH = 22;
//       // 1. 进行分割，获取前三行
//       let c_temps = title.split('\r\n');
//       let line_counts = 0;
//       // 如果超过三行，那么只取前三行
//       if (c_temps.length > 3) {
//         c_temps = c_temps.slice(0, 3);
//       }

//       for (let i in c_temps) {
//         const c_data = c_temps[i];
//         // 计算要换多少行
//         const c_lines = parseInt(c_data.length / NUMBER_OF_LINE) + 1;
//         for (let j = 0; j < c_lines; j++) {
//           startDraw(c_data.slice(j * NUMBER_OF_LINE, (j + 1) * NUMBER_OF_LINE), line_counts);
//           line_counts++;
//         }
//       }

//       // 开始绘制文字
//       // text绘制的文字，line在第几行
//       function startDraw(text, line) {
//         // 如果line > 3，则忽略
//         // 因为line从0开始
//         // 如果最后一行，并且文字还是那么多，那么就省略号代替
//         if (line === 2 && text.length === NUMBER_OF_LINE) {
//           text = text.slice(0, 13) + '..';
//         } else if (line > 2) {
//           return;
//         }
//         const y = 450 + (line * 35); // 200为文字初始y坐标
//         console.log('[draw]', text, y);
//         ctx.setFontSize(20);
//         ctx.setTextAlign('center');
//         ctx.setFillStyle('#333333');
//         for (let i in text) {
//           const t = text[i];
//           // 开始绘制
//           ctx.fillText(t, 50 + (i * FONT_WIDTH), y);
//         }
//       }

//       // 商家／联系方式
//       ctx.setFontSize(12);
//       ctx.setTextAlign('center');
//       ctx.setFillStyle('#888888');
//       ctx.fillText(publisherName + '/' + publisherPhone, 414 / 2, 260);
//       //分享描述
//       ctx.setFontSize(16);
//       ctx.setTextAlign('center');
//       ctx.setFillStyle('#999999');
//       ctx.fillText('长按图片保存分享给朋友', 414 / 2, 550);
//       ctx.fillText('朋友识别图片二维码即可关注该小程序', 414 / 2, 570);
//       // 时间
//       const dates = shipTime.split('-');
//       // 日
//       ctx.setFontSize(60);
//       ctx.setTextAlign('center');
//       ctx.setFillStyle('#666666');
//       ctx.fillText(dates[2].split(' ')[0], 414 / 2, 350);
//       // 月
//       ctx.setFontSize(18);
//       ctx.setTextAlign('center');
//       ctx.setFillStyle('#999999');
//       ctx.fillText(dates[1] + ' / ' + dates[0], 414 / 2, 390);

//       ctx.stroke();

//       ctx.draw();

//       setTimeout(() => RES(), 1000);
//     })).then(() => {
//       // 导出图片
//       wx.hideLoading();
//       wx.canvasToTempFilePath({
//         canvasId: 'shareCanvas',
//         x: 0,
//         y: 0,
//         width: 414,
//         height: 736,
//         success: ret => {
//           this.SHARE_IMG = ret.tempFilePath;
//           // 判断是否是第一次分享，如果是，则显示帮助分享图片，否则只显示分享图片
//           const urls = [ret.tempFilePath];
//           if (IS_FIRST_SHARE < 3) {
//             wx.setStorageSync('share_count', IS_FIRST_SHARE + 1);
//           }
//           wx.previewImage({
//             urls
//           });
//         }
//       }, this)
//     });
//   }


})