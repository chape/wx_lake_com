// pages/auth/auth.js
'use strict';

import api from '../../utils/api';
import util from '../../utils/index';
//获取应用实例
const app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
    code:''
  },

  bindGetUserInfo: function (userResult) {
    var that = this;
    console.log("--1---",userResult)
    var userDetail = userResult.detail;
    let encryptedData = userDetail.encryptedData;
    let iv = userDetail.iv;
    wx.request({
      url: api.postUserUrl,
      method: 'POST',
      header: {
        'content-type': 'application/json',
        'x-auth-token': wx.getStorageSync('sessionId')
      },
      data: {
        encryptedData: encryptedData,
        iv: iv
      },
      success: (res) => {
        if (res.data.errorCode === 0) {
          wx.hideLoading();
          app.globalData.userInfo = res.data.data;
        }
      }
    })

    wx.navigateBack({
     delta:1
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
  
})