'use strict';

import api from '../../utils/api';
import util from '../../utils/index';
//获取应用实例
const app = getApp()

Page({
  data: {
    winHeight: "",//窗口高度
    currentTab: 0, //预设当前项的值
    scrollLeft: 0, //tab标题的滚动条位置
    buyOrderList: [], //已买列表
    sellOrderList: [], //已卖列表
    isx: /iphone10|iphone x/i.test(wx.getSystemInfoSync().model),
    isAndroid: /android/i.test(wx.getSystemInfoSync().system)
  },
  // 滚动切换标签样式
  switchTab: function (e) {
    this.setData({
      currentTab: e.detail.current
    });
    this.renderOrder(this.data.currentTab);
    this.checkCor();
  },
  // 渲染列表数据
  renderOrder: function (tabIndex) {
    console.log("---11",tabIndex)
    var tabIndex = this.data.currentTab;
    if (app.globalData.userInfo) {
      let userId = app.globalData.userInfo.id;
      //已买
      if (0 === tabIndex) {
        util.request({
          url: util.templateFunc(api.buyOrderUrl, { userId: userId }),
          header: {
            'content-type': 'application/json',
            'x-auth-token': wx.getStorageSync('sessionId')
          },
        }).then(res => {
          if (res && res.data) {
            console.log(res.data)
            this.setData({
              buyOrderList: res.data
            });
          }
        })
        //已卖
      } else {
        util.request({
          url: util.templateFunc(api.sellOrderUrl, { userId: userId }),
          header: {
            'content-type': 'application/json',
            'x-auth-token': wx.getStorageSync('sessionId')
          },
        }).then(res => {
          if (res && res.data) {
            console.log(res.data)
            this.setData({
              sellOrderList: res.data
            });
          }
        })
      }
    }
  },
  // 点击标题切换当前页时改变样式
  swichNav: function (e) {
    var cur = e.target.dataset.current;
    if (this.data.currentTaB == cur) {
      return false;
    }
    else {
      this.setData({
        currentTab: cur
      })
    }
  },
  //判断当前滚动超过一屏时，设置tab标题滚动条。
  checkCor: function () {
    if (this.data.currentTab > 4) {
      this.setData({
        scrollLeft: 300
      })
    } else {
      this.setData({
        scrollLeft: 0
      })
    }
  },
  //页面加载时触发
  onLoad: function (options) {
    var that = this;
    options.currentTab === '0' ? wx.setNavigationBarTitle({ title: '我已买到' }) : wx.setNavigationBarTitle({ title: '我卖出的' });
    this.renderOrder(options.currentTab);
    //  高度自适应
    wx.getSystemInfo({
      success: function (res) {
        var clientHeight = res.windowHeight,
          clientWidth = res.windowWidth,
          rpxR = 750 / clientWidth;
        // var calc=clientHeight*rpxR-180;
        var calc = clientHeight * rpxR - 180;
        that.setData({
          winHeight: calc,
          currentTab: options.currentTab
        });
        that.renderOrder(that.data.currentTab);
      }
    })
  },


  callPhone(e) {
    let dataset = e.currentTarget.dataset
    let item = dataset && dataset.item
    console.log(dataset)
    wx.makePhoneCall({
      phoneNumber: item + ''
    })
  },



  // 一键复制事件
  copy: function (e) {
    var that = this;
    wx.setClipboardData({
      //准备复制的数据
      data: e.currentTarget.dataset.text,
      success: function (res) {
        wx.showToast({
          title: '复制成功',
        });
      }
    });
  },

})