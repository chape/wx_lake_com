'use strict';



import api from '../../utils/api';
import util from '../../utils/index';
//获取应用实例
const app = getApp()

Page({

  data: {
  },

  bindFormSubmit:function (e) {
    var content = e.detail.value.textarea;
    console.log(content)
    if(content) {
      util.request({
        url: api.postFeedbackUrl,
        method: 'POST',
        header: {
          'content-type': 'application/json',
          'x-auth-token': wx.getStorageSync('sessionId')
        },
        data: {
          description: content
        },
      }).then(res => {
        if (res && res.errorCode === 0) {
          wx.showToast({
            title: '提交成功',
            icon: 'success',
            duration: 1000
          })

          setTimeout(function () {
            // 跳转至个人中心
            wx.switchTab({
              url: "/pages/mine/mine"
            });
          }, 1000)


        } else {
          wx.showToast({
            title: '服务器异常',
            icon: 'fail',
            duration: 1000
          })
        }
      })
    } else {
      wx.showToast({
        title: '内容为空',
        icon: 'fail',
        duration: 1000
      })
    }
  },




})