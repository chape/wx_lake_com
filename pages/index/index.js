'use strict';

import util from '../../utils/index';
import config from '../../utils/config';
import api from '../../utils/api';

let app = getApp();
let isDEV = config.isDev;

// 后继的代码都会放在此对象中
Page({

    data: {
        lunboList: [], // 存放轮播图数据，与视图相关联
        noticeList: [],
        internal: 3000,
        duration: 1000,
        shopList: [], // 存放店铺列表数据
        hasMore: true,// 用来判断下拉加载更多内容操作
        page: 0, //当前加载第几页的数据
        pageSize: 4,
        totalSize: 0
    },
    onLoad(options) {
        if (options.id) {
            setTimeout(function () {
                wx.navigateTo({
                    url: '/pages/detail/detail?id=' + options.id
                })
            }, 800)
        }

        this.setData({
            hiddenLoading: false,
        }),
            this.requestLunbo(),
            this.requestNotice(),
            this.requestShop()
    },
    /*
     * 获取轮播图数据
     */
    requestLunbo() {
        util.request({
            url: api.homepageProductRecommend,
            method: 'GET',
            header: {
                'content-type': 'application/json',
                'x-auth-token': wx.getStorageSync('sessionId')
            },
            data: {}
        })
            .then(res => {
                // 数据正常返回
                if (res && res.errorCode === 0 && res.data && res.data.length) {
                    let lunboData = res.data;
                    this.renderLunbo(lunboData)
                }
                /*
                 * 返回异常错误
                 * 展示后端返回的错误信息，并设置下拉加载功能不可用
                 */
                else {
                    this.setData({
                        lunboList: app.globalData.defaultLunboData
                    });
                    return null;
                }
            });
    },
    renderLunbo(data) {
        if (data && data.length) {
            this.setData({
                lunboList: data
            })
        }
    },

    /*
     * 获取店铺列表数据
     */
    requestShop() {
        util.request({
            url: api.homepageProductUrl,
            method: 'GET',
            header: {
                'content-type': 'application/json',
                'x-auth-token': wx.getStorageSync('sessionId')
            },
            data: {
                page: this.data.page || 0,
                size: this.data.size || 3,
            }
        })
            .then(res => {
                // 数据正常返回
                console.log(res.data.data)
                this.setData({
                    shopList: res.data.data
                })

            })
    },



    requestNotice(cb) {
        util.request({
            url: api.noticePageUrl,
            method: 'GET',
            header: {
                'content-type': 'application/json',
                'x-auth-token': wx.getStorageSync('sessionId')
            },
            data: {
                page: this.data.page || 0,
                size: this.data.size || 5,
            }
        })
            .then(res => {
                console.log(res.data.data)
                this.setData({
                    noticeList: res.data.data
                })
            });
    },


    /*
     * 分享
     */
    onShareAppMessage() {
        let title = config.defaultShareText || '';
        return {
            title: title,
            path: `/pages/index/index`,
            success: function (res) {
                wx.showToast({
                    title: '分享成功',
                    icon: 'success',
                    duration: 1000
                })
            },
            fail: function (res) {
                // 转发失败
            }
        }
    },

    imageLoad: function () {
        this.setData({
            imageWidth: wx.getSystemInfoSync().windowWidth
        })
    },
    goDeatil: function (e) {
        wx.navigateTo({
            url: '/pages/details/detail?id=' + e.currentTarget.dataset.id,  //跳转页面的路径，可带参数 ？隔开，不同参数用 & 分隔；相对路径，不需要.wxml后缀
            success: function () {
            }        //成功后的回调；
        })
    },

    goNoticeList:function () {
        wx.switchTab({
            url: '/pages/notice/notice',   //注意switchTab只能跳转到带有tab的页面，不能跳转到不带tab的页面
        })
    }




})