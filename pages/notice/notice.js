import util from '../../utils/index';
import config from '../../utils/config';
import api from '../../utils/api';


const app = getApp()

Page({
    data: {
        full: true,
        noticeList: [], // 存放通知列表数据
        hasMore: true,// 用来判断下拉加载更多内容操作
        page: 0, //当前加载第几页的数据
        pageSize: 4,
        totalSize: 0
    },

    onLoad() {
        this.setData({
            hiddenLoading: false,
        }),
        this.requestNotice()
    },


    requestNotice(cb) {
        util.request({
            url: api.noticePageUrl,
            method: 'GET',
            header: {
                'content-type': 'application/json',
                'x-auth-token': wx.getStorageSync('sessionId')
            },
            data: {
                page: this.data.page || 0,
                size: this.data.size || 1000,
            }
        })
            .then(res => {
                console.log(res.data.data)
               this.setData({
                   noticeList:res.data.data
               })
            });
    },


    // 跳转到详情页
    // goTodetails:function(e){
    //     var details = JSON.stringify(e.currentTarget.dataset.item);
    //     console.log(details)
    //     wx.navigateTo({
    //         url: "/pages/notice/detail/detail?details=" + details
    //     })
    // }




})