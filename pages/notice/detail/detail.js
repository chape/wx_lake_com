import util from '../../../utils/index';
import config from '../../../utils/config';
import api from '../../../utils/api';
var WxParse = require('../../../components/wxParse/wxParse.js');
Page({
    data: {
        html:'',
        obj:''
    },
    onLoad: function (options) {


        /**
         * 初始化emoji设置
         */
        WxParse.emojisInit('[]', "../../../components/wxParse/emojis/", {
            "00": "00.gif",
            "01": "01.gif",
            "02": "02.gif",
            "03": "03.gif",
            "04": "04.gif",
            "05": "05.gif",
            "06": "06.gif",
            "07": "07.gif",
            "08": "08.gif",
            "09": "09.gif",
            "09": "09.gif",
            "10": "10.gif",
            "11": "11.gif",
            "12": "12.gif",
            "13": "13.gif",
            "14": "14.gif",
            "15": "15.gif",
            "16": "16.gif",
            "17": "17.gif",
            "18": "18.gif",
            "19": "19.gif",
        });



        var that = this;
        util.request({
            url: util.templateFunc(api.notificationDetails, { id: options.id })
        }).then(res=>{
            that.setData({
                obj: res.data,
                html: res.data.description
            });

            WxParse.wxParse('article', 'html', res.data.description, that, 0);
        })


        //
        // var article = that.data.html;
        //
        // console.log(article)

    }


})
