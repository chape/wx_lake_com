import api from '../../utils/api';
import util from '../../utils/index';
import config from "../../utils/config";
//获取应用实例
const app = getApp()


Page({
    data: {
        internal: 3000,
        duration: 1000,
        shopList: [], // 存放店铺列表数据
        hasMore: true,// 用来判断下拉加载更多内容操作
        page: 0, //当前加载第几页的数据
        pageSize: 4,
        totalSize: 0,
        productType: 0,
    },
    onLoad(options) {
        console.log(options)
        this.setData({
            hiddenLoading: false,
            productType:options.productType
        }),
            this.requestShop()
    },

    /*
     * 获取店铺列表数据
     */
    requestShop(cb) {
        console.log(this.data.productType)
        util.request({
            url: api.homepageProductUrl,
            method: 'GET',
            header: {
                'content-type': 'application/json',
                'x-auth-token': wx.getStorageSync('sessionId')
            },
            data: {
                page: this.data.page || 0,
                size: this.data.size || 15,
                category: this.data.productType
            }
        })
            .then(res => {

                // 数据正常返回
                if (res && res.errorCode === 0 && res.data && res.data.data && res.data.data.length) {
                    let shopData = res.data.data;
                    this.renderShop(shopData)
                }
                /*
                * 如果加载第一页就没有数据，说明数据存在异常情况
                * 处理方式：弹出异常提示信息（默认提示信息）并设置下拉加载功能不可用
                */
                else if (this.data.page === 0 && res.data && res.data.data && res.data.data.length === 0) {
                  //  util.alert();
                    console.log(res)
                    this.setData({
                        hasMore: false
                    });
                }
                /*
                * 如果非第一页没有数据，那说明没有数据了，停用下拉加载功能即可
                */
                else if (this.data.page !== 0 && res.data && res.data.data && res.data.data.length === 0) {
                    this.setData({
                        hasMore: false
                    });
                }
                /*
                * 返回异常错误
                * 展示后端返回的错误信息，并设置下拉加载功能不可用
                */
                else {
                    util.alert('提示', res);
                    console.log(res)
                    this.setData({
                        hasMore: false
                    });
                    return null;
                }
                cb && cb();
            });
    },
    renderShop(data) {
        if (this.data.page === 0) {
            this.setData({
                shopList: []
            })
        }
        if (data && data.length) {
            let newList = this.data.shopList.concat(data);
            this.setData({
                shopList: newList,
                hiddenLoading: true
            })
        }
    },
    onPullDownRefresh() {
      this.data.page = 0;
      this.data.hasMore = true;
      this.requestShop(function() {
        wx.stopPullDownRefresh();
      });
    },
    /*
    * 每次触发，我们都会先判断是否还可以『加载更多』
    * 如果满足条件，那说明可以请求下一页列表数据，这时候把 data.page 累加 1
    * 然后调用公用的请求函数
    */
    onReachBottom() {
      if (this.data.hasMore) {
        let nextPage = this.data.page + 1;
        this.setData({
          page: nextPage
        });
        this.requestShop();
      }
    },

    callPhone(e) {
        let dataset = e.currentTarget.dataset
        let item = dataset && dataset.item
        wx.makePhoneCall({
            phoneNumber: item+''
        })
    },

    /*
    * 分享
    */
    onShareAppMessage() {
        let title = config.defaultShareText || '';
        return {
            title: title,
            path: `/pages/index/index`,
            success: function (res) {
                wx.showToast({
                    title: '分享成功',
                    icon: 'success',
                    duration: 1000
                })
            },
            fail: function (res) {
                // 转发失败
            }
        }
    },

    imageLoad: function () {
        this.setData({
            imageWidth: wx.getSystemInfoSync().windowWidth
        })
    },

    goDeatil: function (e) {
        wx.navigateTo({
            url:'/pages/details/detail?id='+e.currentTarget.dataset.id,  //跳转页面的路径，可带参数 ？隔开，不同参数用 & 分隔；相对路径，不需要.wxml后缀
            success:function(){}        //成功后的回调；
        })
    }
})