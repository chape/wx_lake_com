// components/qrcode-share/index.js
import api from '../../utils/api';
import util from '../../utils/index';

const DEFAULTDATA = {
  imgUrl: 'http://img.lake2716.cn/d8d38e07-515e-4ddd-8d90-5d91461d0bb04040838051649001435.jpg',
  title: '邻满满服务平台',
  shipTime: '2018-08-20 00:00:00',
  publisherName: '小满',
  publisherPhone: '15882774298'
}

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    detailInfo: {
      type: Object,
      value: {}
    },
    scene: {
      type: String,
      value: ''
    },
    pages: {
      type: String,
      value: ''
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    // 分享
    shareHandler: function (e) {
      // 如果已经生成了，那么就显示
      if (this.SHARE_IMG) {
        return wx.previewImage({
          urls: [this.SHARE_IMG],
        })
      }
      wx.showLoading({
        title: '生成图片中',
        mask: true
      });
      const ctx = wx.createCanvasContext('shareCanvas', this);
      // ctx.save();
      ctx.drawImage('/imgs/share_tpl2@2x.png', 0, 0, 414, 736);
      //ctx.draw();
      if(this.data.detailInfo === null){
        this.setData({
          detailInfo: DEFAULTDATA
        })
      }
      const {
        imgUrl, title, shipTime,
        publisherName, publisherPhone
      } = this.data.detailInfo;
      new Promise(RES => {
        // 下载图片
        wx.getImageInfo({
          src: imgUrl,
          success: ret => {
            ctx.drawImage(ret.path, 0, 0, 414, 290);
            // 渲染模板图片
            ctx.drawImage('/imgs/box@2x.png', 0, 174, 414, 562);
            RES();
          }
        })
      }).then(() => new Promise(RES => {
        util.request({
          url: api.getQRCodeUrl,
          method: 'GET',
          header: {
            'content-type': 'application/json',
            'x-auth-token': wx.getStorageSync('sessionId')
          },
          data: {
            scene: decodeURIComponent(this.data.scene),
            pages: this.data.pages
          }
        })
          .then(res => {
            // 数据正常返回
            if (res && res.data) {
              // 下载动态二维码
              wx.getImageInfo({
                src: res.data,
                success: ret => {
                  // 渲染二维码
                  ctx.drawImage(ret.path, 167, 618, 80, 80);
                  RES();
                }
              })
            }
          })

      })).then(() => new Promise(RES => {
        // 绘制文字

        // 文字
        /**
         * 渲染主体内容思路：
         * 
         * 首先，通过\r\n进行分割，获取到每一行。
         * 然后，每一行进行每20*30个方格的计算，多出来的换到下一行，不满足的用空格填充
         * 综上，一共获取到前三行（换行过长算作下一行）—
         */

        // 一行多少字
        const NUMBER_OF_LINE = 15;
        // 每个字多宽
        const FONT_WIDTH = 20;
        // 1. 进行分割，获取前三行
        let c_temps = title.split('\r\n');
        let line_counts = 0;
        // 如果超过三行，那么只取前三行
        if (c_temps.length > 3) {
          c_temps = c_temps.slice(0, 3);
        }

        for (let i in c_temps) {
          const c_data = c_temps[i];
          // 计算要换多少行
          const c_lines = parseInt(c_data.length / NUMBER_OF_LINE) + 1;
          for (let j = 0; j < c_lines; j++) {
            startDraw(c_data.slice(j * NUMBER_OF_LINE, (j + 1) * NUMBER_OF_LINE), line_counts);
            line_counts++;
          }
        }

        // 开始绘制文字
        // text绘制的文字，line在第几行
        function startDraw(text, line) {
          // 如果line > 3，则忽略
          // 因为line从0开始
          // 如果最后一行，并且文字还是那么多，那么就省略号代替
          if (line === 2 && text.length === NUMBER_OF_LINE) {
            text = text.slice(0, 13) + '..';
          } else if (line > 2) {
            return;
          }
          const y = 450 + (line * 35); // 200为文字初始y坐标
          console.log('[draw]', text, y);
          ctx.setFontSize(20);
          ctx.setTextAlign('center');
          ctx.setFillStyle('#333333');
          for (let i in text) {
            const t = text[i];
            // 开始绘制
            ctx.fillText(t, 65 + (i * FONT_WIDTH), y);
          }
        }

        // 商家／联系方式
        ctx.setFontSize(12);
        ctx.setTextAlign('center');
        ctx.setFillStyle('#888888');
        ctx.fillText(publisherName + '/' + publisherPhone, 414 / 2, 260);
        //分享描述
        ctx.setFontSize(16);
        ctx.setTextAlign('center');
        ctx.setFillStyle('#999999');
        ctx.fillText('长按图片保存分享给朋友', 414 / 2, 550);
        ctx.fillText('朋友识别图片二维码即可关注该小程序', 414 / 2, 570);
        // 时间
        const dates = shipTime.split('-');
        // 日
        ctx.setFontSize(60);
        ctx.setTextAlign('center');
        ctx.setFillStyle('#666666');
        ctx.fillText(dates[2].split(' ')[0], 414 / 2, 350);
        // 月
        ctx.setFontSize(18);
        ctx.setTextAlign('center');
        ctx.setFillStyle('#999999');
        ctx.fillText(dates[1] + ' / ' + dates[0], 414 / 2, 390);

        ctx.stroke();

        ctx.draw();

        setTimeout(() => RES(), 1000);
      })).then(() => {
        // 导出图片
        wx.hideLoading();
        wx.canvasToTempFilePath({
          canvasId: 'shareCanvas',
          x: 0,
          y: 0,
          width: 414,
          height: 736,
          success: ret => {
            this.SHARE_IMG = ret.tempFilePath;
            const urls = [ret.tempFilePath];
            wx.previewImage({
              urls
            });
          }
        }, this)
      });
    }
  }
})
